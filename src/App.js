import React, { useState, useEffect } from 'react';
import UsersList from './components/UsersList';
import AddUserForm from './components/AddUserForm';

import './styles/bulma.min.css';
import './styles/app.css';

export default function App() {
	const [users, setUsers] = useState([]);

	useEffect(() => {
		const fetchUsers = async () => {
			const response = await fetch('https://jsonplaceholder.typicode.com/users');
			const users = await response.json();

			setUsers(users);
		};

		fetchUsers();
	}, []);

	function addUser(newUser) {
		setUsers([...users, newUser]);
	}

	return (
		<article>
			<section id="hero" className="hero is-info">
				<div className="hero-body">
					<div className="container">
						<h1 className="title is-1">React Front-End Dev Test</h1>
					</div>
				</div>
			</section>

			<div id="body" className="container">
				<div className="columns">
					<section id="users-list" className="column">
						<div className="container">
							<h2 className="title is-2">Users <small className="is-size-5">({users.length})</small></h2>
							<UsersList users={users} />
						</div>
					</section>

					<section id="add-user" className="column">
						<div className="container">
							<h2 className="title is-2">Add User</h2>
							<AddUserForm addUser={addUser}/>
						</div>
					</section>
				</div>
			</div>

			<section id="footer">
				<footer className="footer">
					<div className="content has-text-centered">
						<p>Developed by <a href="mailto:me@dlwiest.com">Derrick L. Wiest</a> (2019) for <a href="https://www.industryweapon.com/">Industry Weapon</a></p>
					</div>
				</footer>
			</section>
		</article>
	);
}
