import React, { useState } from "react";

export default function AddUserForm({ addUser }) {
	const [form, setForm] = useState({
		name: '',
		email: '',
	});
	const [didPressSubmit, setDidPressSubmit] = useState(false);


	function onSubmit(e) {
		e.preventDefault();
		setDidPressSubmit(true);

		if (validateName() && validateEmail()) {
			addUser(form);

			setForm({
				name: '',
				email: '',
			});
			setDidPressSubmit(false);
		}
	}

	function setField(e) {
		setForm({
			...form,
			[e.target.name]: e.target.value,
		});
	}

	function validateName() {
		return /^[A-Za-z ,.'-]+ [A-Za-z ,.'-]+$/.test(form.name);
	}

	function validateEmail() {
		return /^[^@]+@[^@]+\.[^@]+$/.test(form.email);
	}

	return (
		<form onSubmit={onSubmit}>
			<div className="field">
				<label className="label">Name</label>
				<div className="control">
					<input
						name="name"
						value={form.name}
						onChange={setField}
						className={didPressSubmit && !validateName() ? 'input is-danger' : 'input'}
						type="text"
						placeholder="First Last"
					/>
				</div>
				{didPressSubmit && !validateName() && <p className="help is-danger">First and last name are required</p>}
			</div>

			<div className="field">
				<label className="label">Email</label>
				<div className="control">
					<input
						name="email"
						value={form.email}
						onChange={setField}
						className={didPressSubmit && !validateEmail() ? 'input is-danger' : 'input'}
						type="text"
						placeholder="user@domain"
					/>
				</div>
				{didPressSubmit && !validateEmail() && <p className="help is-danger">A valid email is required</p>}
			</div>

			<button className="button is-primary" type="submit">Add User</button>
		</form>
	);
}