import React, { useState } from "react";

export default function UsersList({ users }) {
	const DISPLAY_PER_PAGE = 5;
	const [start, setStart] = useState(0);

	function getAlphabetized() {
		return users.sort((a, b) => {
			if (a.name.toUpperCase() < b.name.toUpperCase()) return -1;
			if (a.name.toUpperCase() > b.name.toUpperCase()) return 1;
			return 0;
		});
	}

	function getVisible() {
		const end = start + DISPLAY_PER_PAGE >= users.length ? users.length : start + DISPLAY_PER_PAGE;
		return getAlphabetized().slice(start, end);
	}

	return (
		<React.Fragment>
			<div className="list">
				{getVisible().map(u => 
					<span key={u.email} className={u.email.endsWith('biz') ? 'list-item has-text-primary' : 'list-item'}>{u.name}</span>
				)}
			</div>
			<button
				className="button is-info"
				onClick={() => setStart(start - DISPLAY_PER_PAGE)}
				disabled={!start}
			>
				Previous
			</button>
			<button
				className="button is-info"
				onClick={() => setStart(start + DISPLAY_PER_PAGE)}
				style={{ marginLeft: 10 }}
				disabled={start + DISPLAY_PER_PAGE + 1 > users.length}
			>
				Next
			</button>
		</React.Fragment>
	);
}